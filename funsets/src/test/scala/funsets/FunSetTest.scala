package funsets

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
@RunWith(classOf[JUnitRunner])
class FunSetTest extends FunSetSuite {
  import FunSets._

  test("intersections") {
    new TestSets {
      val setA = intersect(s1, s2)
      val setB = intersect(s1, s1)
      assert(!contains(setA, 1))
      assert(!contains(setA, 2))
      assert(contains(setB, 1))
    }
  }
  
  test("forall") {
    new TestSets {
      val s = union(s1, s2)
      
      assert(forall(s, (x => x < 4)))
      assert(!forall(s, (x => x < 2)))
    }
  }
  
  test("exists"){
    new TestSets{
      assert(exists(s1, s1))
      assert(!exists(s1, s2))
    }    
  }
  
  test("mapping"){
    new TestSets{
      val setTest = union(s1, s2)
      val value = map(setTest, x => x * x)
      assert(contains(value, 1))
      assert(contains(value, 4))
    }
  }

}