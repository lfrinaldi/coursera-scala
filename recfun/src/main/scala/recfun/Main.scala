package recfun
import common._
import scala.annotation.tailrec
import scala.collection.mutable.Stack

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || c == r) return 1
    return pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  val stack = new Stack[Char]
  def balance1(chars: List[Char]): Boolean = {
    def process(x: Char, l: List[Char]): Stack[Char] = {
      if (x == '(') { stack.push(x) }
      else if (x == ')') {
        if (stack.isEmpty) {
          stack.push(x)
        } else stack.pop()
      }
      if (l.isEmpty) return stack;
      return process(l.head, l.tail)
    }

    return process(chars.head, chars.tail).isEmpty
  }

  def balance(chars: List[Char]): Boolean = {
    def balanced(chars: List[Char], count: Int = 0): Boolean =
      chars match {
        case Nil      => count == 0
        case '(' :: f => balanced(f, count + 1) //avanzo en uno
        case ')' :: f => count != 0 && balanced(f, count - 1) //retrocedo
        case _ :: f   => balanced(f, count)
      }
    balanced(chars)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    def count(money: Int, coins: List[Int]): Int = {
      if (coins.isEmpty) 0
      else if (money - coins.head == 0) 1
      else if (money - coins.head < 0) 0
      else countChange(money - coins.head, coins) + countChange(money, coins.tail);
    }
    count(money, coins.sorted)
  }
}
